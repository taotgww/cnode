// pages/details/details.js
const http = require('../../http/index.js').http
const WxParse = require('../../lib/wxParse/wxParse.js')
const moment = require('../../lib/moment.min.js')
moment.locale('zh-cn')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    details: null,
    loaded: false,
    token: ''
  },
  // 获取详情
  getDetails(id) {
    let token = wx.getStorageSync('token') || ''
    this.setData({
      loaded: false
    })
    http('/topic/' + id, {
        mdrender: true,
        accesstoken: token
      })
      .then(res => {
        let details = res.data
        let article = res.data.content
        res.data.create_at = moment(res.data.create_at).fromNow()
        this.setData({
          details,
          loaded: true
        })
        var that = this;
        WxParse.wxParse('article', 'html', article, that, 5);
      })
  },
  onLoad(options) {
    const token = wx.getStorageSync('token') || ''
    let id = options.id
    this.getDetails(id)
    this.setData({
      token: token
    })
  },
  // 收藏
  collect(e) {
    let topic_id = e.currentTarget.id
    if (this.data.token) {
      http('/topic_collect/collect', {
          accesstoken: this.data.token,
          topic_id
        }, 'POST')
        .then(res => {
          wx.showToast({
            title: '收藏成功',
          })
          // setTimeout(()=>{
          //   this.getDetails(topic_id)
          // },1500)
          this.setData({
            'details.is_collect': true
          })
        })
      return
    }
    wx.showModal({
      title: '登录提示',
      content: '您还没有登录，是否去登录页面？',
      success: (data) => {
        if (data.confirm) {
          wx.navigateTo({
            url: '/pages/login/login',
          })
        } else if (data.cancel) {
          // console.log('用户点击取消')
        }
      }
    })
  },
  // 取消收藏
  cancelCollect(e) {
    let topic_id = e.currentTarget.id
    http('/topic_collect/de_collect', {
        accesstoken: this.data.token,
        topic_id
      },'POST')
      .then(res => {
        wx.showToast({
          title: '取消收藏',
          icon:'none'
        })
        this.setData({
          'details.is_collect': false
        })
      })
  }
})