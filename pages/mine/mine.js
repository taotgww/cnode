// pages/mine/mine.js
const http = require('../../http/index.js').http
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: null
  },
  onShow() {
    let token = wx.getStorageSync('token');
    if (!token) {
      wx.redirectTo({
        url: '/pages/login/login',
      })
      return
    }
    let userInfo = JSON.parse(wx.getStorageSync('userInfo'))
    this.setData({
      userInfo: userInfo
    })
    http('/accesstoken', {
        accesstoken: token
      }, 'POST')
      .then(res => {
        let userInfo = JSON.stringify(res)
        this.setData({
          userInfo: res
        })
        wx.setStorageSync('userInfo', userInfo)
      })
  },
  toLogout() {
    wx.navigateTo({
      url: '/pages/logout/logout',
    })
  },
  //跳转收藏页面
  toCollect() {
    wx.navigateTo({
      url: '/pages/collect/collect',
    })
  },
  //跳转消息页面
  toMessage() {
    wx.navigateTo({
      url: '/pages/message/message',
    })
  },
  //跳转设置页面
  toSetting() {
    wx.navigateTo({
      url: '/pages/setting/setting',
    })
  },
  //跳转关于页面
  toAbout() {
    wx.navigateTo({
      url: '/pages/about/about',
    })
  }
})