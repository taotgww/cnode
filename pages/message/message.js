// pages/message/message.js
const http = require('../../http/index.js').http
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[]

  },
  // 跳转主页面
  toIndex() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },

})