// pages/collect/collect.js
const http = require('../../http/index.js').http
const moment = require('../../lib/moment.min.js')
moment.locale('zh-cn')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lists: [],
    loaded: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getCollectLists()
  },
  //获取收藏列表
  getCollectLists() {
    let userInfo = JSON.parse(wx.getStorageSync('userInfo'))
    wx.setNavigationBarTitle({
      title: userInfo.loginname + '的收藏',
    })
    http('/topic_collect/' + userInfo.loginname)
      .then(res => {
        for (let i = 0, len = res.data.length; i < len; i++) {
          res.data[i].create_at = moment(res.data[i].create_at).fromNow()
          res.data[i].last_reply_at = moment(res.data[i].last_reply_at).fromNow()
        }
        console.log(res)
        this.setData({
          lists: res.data,
          loaded: true
        })
      })
  }

})